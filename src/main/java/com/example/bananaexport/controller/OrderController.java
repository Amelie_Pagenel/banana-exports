package com.example.bananaexport.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bananaexport.entity.Order;
import com.example.bananaexport.service.OrderService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("Orders")
@RestController
@RequestMapping(path = "orders")
public class OrderController {

	@Autowired
	private OrderService orderService;

	@ApiOperation(value = "getOrdersByReceiverId")
	@GetMapping(path = "/{id}")
	public List<Order> getOrdersByReceiverId(@PathVariable Long id) {
		return orderService.getOrdersByCompanyId(id);
	}

	@ApiOperation(value = "addOrder")
	@PostMapping()
	public Order addOrder(@RequestBody Order order) {
		return orderService.addOrder(order);
	}

	@ApiOperation(value = "updateOrder")
	@PutMapping(path = "/{id}")
	public Order updateOrder(@PathVariable Long id, @RequestBody Order order) {
		return orderService.updateOrder(id, order);
	}

	@ApiOperation(value = "deleteOrder")
	@DeleteMapping(path = "/{id}")
	public void deleteOrder(@PathVariable Long id) {
		orderService.deleteOrder(id);
	}

}
