package com.example.bananaexport.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bananaexport.entity.Order;
import com.example.bananaexport.exception.NotFoundException;
import com.example.bananaexport.exception.BadRequestException;
import com.example.bananaexport.repository.OrderRepository;
import com.example.bananaexport.utils.OrderUtils;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private OrderUtils orderUtils;

	public List<Order> getOrdersByCompanyId(Long id) {
		return orderRepository.findByCompanyId(id);
	}

	public Order addOrder(Order order) {
		boolean isQuantityValid = orderUtils.isQuantityValid(order.getQuantity());

		if (isQuantityValid == false) {
			throw new BadRequestException("quantity-is-not-valid");
		}

		Date deliveryDate = orderUtils.calculateDeliveryDate(new Date());

		Order newOrder = new Order();
		newOrder.setDeliveryDate(deliveryDate);
		newOrder.setQuantity(order.getQuantity());
		newOrder.setCompanyId(order.getCompanyId());
		newOrder.setPrice(orderUtils.calculateDeliveryPrice(order.getQuantity()));

		return orderRepository.save(newOrder);

	}

	public Order updateOrder(Long id, Order order) {
		Optional<Order> orderInDb = orderRepository.findById(id);

		if (!orderInDb.isPresent()) {
			throw new NotFoundException("no-order-found - id : " + id);
		}

		boolean isQuantityValid = orderUtils.isQuantityValid(order.getQuantity());

		if (isQuantityValid == false) {
			throw new BadRequestException("quantity-is-not-valid - order id : " + id);
		}

		Order orderToUpdate = orderInDb.get();
		orderToUpdate.setPrice(orderUtils.calculateDeliveryPrice(order.getQuantity()));
		orderToUpdate.setQuantity(order.getQuantity());

		return orderRepository.save(orderToUpdate);

	}

	public void deleteOrder(Long id) {
		Optional<Order> orderInDb = orderRepository.findById(id);

		if (!orderInDb.isPresent()) {
			throw new NotFoundException("no-order-found - id : " + id);
		}
		orderRepository.deleteById(id);
	}

}
