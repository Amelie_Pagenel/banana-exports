package com.example.bananaexport.service;

import java.util.List;

import com.example.bananaexport.entity.Order;

public interface OrderService {

	public List<Order> getOrdersByCompanyId(Long id);

	public Order addOrder(Order order);

	public Order updateOrder(Long Id, Order order);

	public void deleteOrder(Long id);

}
