package com.example.bananaexport.service;

import java.util.List;
import java.util.Optional;

import com.example.bananaexport.entity.Company;

public interface CompanyService {

	public List<Company> getCompanies();

	public Company addCompany(Company company);

	public Optional<Company> updateCompany(Long id, Company company);

	public void deleteCompany(Long id);

}
