package com.example.bananaexport.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bananaexport.entity.Company;
import com.example.bananaexport.exception.BadRequestException;
import com.example.bananaexport.exception.NotFoundException;
import com.example.bananaexport.repository.CompanyRepository;

@Service
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private CompanyRepository companyRepository;

	
	public List<Company> getCompanies() {
		return companyRepository.findAll();
	}

	
	public Company addCompany(Company company) {
		Optional<Company> companyInDb = companyRepository.findByNameAndAddressAndPostalCodeAndCityAndCountry(
				company.getName(), company.getAddress(), company.getPostalCode(), company.getCity(),
				company.getCountry());

		if (companyInDb.isPresent()) {
			throw new BadRequestException("company-already-exists");
		}
		return companyRepository.save(company);

	}

	
	public Optional<Company> updateCompany(Long id, Company updatedCompany) {
		Optional<Company> companyInDb = companyRepository.findById(id);
		if (!companyInDb.isPresent()) {
			throw new NotFoundException("no-company-found - company id : " + id);
		}
		return companyInDb.map(company -> {
			company.setName(updatedCompany.getName());
			company.setAddress(updatedCompany.getAddress());
			company.setPostalCode(updatedCompany.getPostalCode());
			company.setCity(updatedCompany.getCity());
			company.setCountry(updatedCompany.getCountry());
			return companyRepository.save(company);
		});
	}

	
	public void deleteCompany(Long id) {
		Optional<Company> companyInDb = companyRepository.findById(id);
		if (!companyInDb.isPresent()) {
			throw new NotFoundException("no-company-found - company id : " + id);
		}
		companyRepository.deleteById(id);
	}

}
