package com.example.bananaexport.exception;


import org.jboss.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class BadRequestException extends ResponseStatusException {

	private static final long serialVersionUID = 1L;
	
	Logger logger = Logger.getLogger(this.getClass());

	public BadRequestException(String message) {
		super(HttpStatus.BAD_REQUEST, message);
		logger.error(message);
	}

}
