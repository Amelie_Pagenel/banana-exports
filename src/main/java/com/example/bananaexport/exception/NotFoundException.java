package com.example.bananaexport.exception;

import org.jboss.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class NotFoundException extends ResponseStatusException {

	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(this.getClass());

	public NotFoundException(String message) {
		super(HttpStatus.NOT_FOUND, message);
		logger.error(message);
	}

}
