package com.example.bananaexport.utils;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class OrderUtilsImpl implements OrderUtils {

	public Date calculateDeliveryDate(Date today) {

		Calendar calendar = Calendar.getInstance();

		calendar.setTime(today);
		calendar.add(Calendar.DATE, 10);

		return calendar.getTime();
	}

	public boolean isQuantityValid(int quantity) {
		return quantity <= 0 || quantity > 10000 || quantity % 25 != 0 ? false : true;
	}

	public double calculateDeliveryPrice(int quantity) {
		return quantity * 2.5;

	}

}
