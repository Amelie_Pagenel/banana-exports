package com.example.bananaexport.utils;

import java.util.Date;

public interface OrderUtils {

	public Date calculateDeliveryDate(Date date);

	public boolean isQuantityValid(int quantity);

	public double calculateDeliveryPrice(int quantity);

}
