package com.example.bananaexport.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.example.bananaexport.entity.Order;
import com.example.bananaexport.service.OrderService;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebMvcTest({ OrderController.class })
public class OrderControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	OrderService orderService;

	Order order = new Order();

	@BeforeEach
	void setUpTest() {

		order.setCompanyId(3L);
		order.setDeliveryDate(new Date());
		order.setQuantity(25);
		order.setPrice(120);

	}

	@Test
	void shouldReturn200OnGetOrdersByReceiverId() throws Exception {

		List<Order> orders = new ArrayList<Order>();
		orders.add(order);

		when(orderService.getOrdersByCompanyId(3L)).thenReturn(orders);

		mockMvc.perform(MockMvcRequestBuilders.get("/orders/{id}", 3L).contentType("application/json"))
				.andExpect(status().isOk());

	}

	@Test
	void shouldReturn200OnAddOrder() throws Exception {

		when(orderService.addOrder(any())).thenReturn(order);

		mockMvc.perform(MockMvcRequestBuilders.post("/orders").content(objectMapper.writeValueAsString(order))
				.contentType("application/json")).andExpect(status().isOk());

	}

	@Test
	void shouldReturn200OnUpdateOrder() throws Exception {

		order.setQuantity(50);

		when(orderService.updateOrder(8L, order)).thenReturn(order);

		mockMvc.perform(MockMvcRequestBuilders.put("/orders/{id}", 8L).content(objectMapper.writeValueAsString(order))
				.contentType("application/json")).andExpect(status().isOk());

	}

	@Test
	void shouldReturn200OnDeleteOrder() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.delete("/orders/{id}", 8L).contentType("application/json"))
				.andExpect(status().isOk());

	}

}
