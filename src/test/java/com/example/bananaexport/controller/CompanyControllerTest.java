package com.example.bananaexport.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.example.bananaexport.entity.Company;
import com.example.bananaexport.service.CompanyService;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

@WebMvcTest({ CompanyController.class })
public class CompanyControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	CompanyService companyService;

	Company company = new Company();

	@BeforeEach
	void setUpTest() {

		company.setName("Test banana import");
		company.setAddress("12 rue des fruits");
		company.setPostalCode("75013");
		company.setCity("Paris");
		company.setCountry("France");

	}

	@Test
	void shouldReturn200OnGetAllCompanies() throws Exception {

		List<Company> companies = new ArrayList<Company>();
		companies.add(company);

		when(companyService.getCompanies()).thenReturn(companies);

		mockMvc.perform(MockMvcRequestBuilders.get("/companies").contentType("application/json"))
				.andExpect(status().isOk());

	}

	@Test
	void shouldReturn200OnAddCompany() throws Exception {

		when(companyService.addCompany(company)).thenReturn(company);
		mockMvc.perform(MockMvcRequestBuilders.post("/companies").content(objectMapper.writeValueAsString(company))
				.contentType("application/json")).andExpect(status().isOk());

	}

	@Test
	void shouldReturn200OnUpdateCompany() throws Exception {

		when(companyService.addCompany(company)).thenReturn(company);
		mockMvc.perform(MockMvcRequestBuilders.put("/companies/{id}", 1L)
				.content(objectMapper.writeValueAsString(company)).contentType("application/json"))
				.andExpect(status().isOk());

	}

	@Test
	void shouldReturn200OnDeleteCompany() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.delete("/companies/{id}", 8L).contentType("application/json"))
				.andExpect(status().isOk());
	}

}
