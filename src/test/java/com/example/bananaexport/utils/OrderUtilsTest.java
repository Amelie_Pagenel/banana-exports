package com.example.bananaexport.utils;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class OrderUtilsTest {

	@InjectMocks
	private OrderUtilsImpl orderUtils;

	@Test
	void shouldCheckIfQuantityIsValid() {

		assertThat(orderUtils.isQuantityValid(1)).isFalse();
		assertThat(orderUtils.isQuantityValid(25)).isTrue();
		assertThat(orderUtils.isQuantityValid(0)).isFalse();
		assertThat(orderUtils.isQuantityValid(10002)).isFalse();

	}

	@Test
	void shouldCalculateDeliveryPrice() {
		assertThat(orderUtils.calculateDeliveryPrice(25)).isEqualTo(62.5);
	}

	@Test
	void shouldCalculateDeliveryDate() {
		Date today = new Date();
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(today);
		calendar.add(Calendar.DATE, 10);

		Date dateToTest = calendar.getTime();

		assertThat(orderUtils.calculateDeliveryDate(today)).isEqualTo(dateToTest);

	}

}
