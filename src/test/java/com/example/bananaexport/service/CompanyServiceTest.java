package com.example.bananaexport.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.bananaexport.entity.Company;
import com.example.bananaexport.exception.BadRequestException;
import com.example.bananaexport.exception.NotFoundException;
import com.example.bananaexport.repository.CompanyRepository;

import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class CompanyServiceTest {

	@Mock
	private CompanyRepository companyRepository;

	@InjectMocks
	private CompanyServiceImpl companyServiceImpl;

	Company company = new Company();

	@BeforeEach
	public void setUpTest() {
		company.setId(3L);
		company.setName("Banana Imports");
		company.setAddress("12 rue de la Paix");
		company.setPostalCode("75013");
		company.setCity("Paris");
		company.setCountry("France");
	}

	@Test
	void shouldReturnAllCompanies() {
		List<Company> companies = new ArrayList<Company>();
		companies.add(company);

		given(companyRepository.findAll()).willReturn(companies);

		List<Company> foundCompanies = companyServiceImpl.getCompanies();
		assertThat(foundCompanies.size()).isEqualTo(1);
	}

	@Test
	void shouldAddCompany() {
		given(companyRepository.save(any(Company.class))).willReturn(company);

		Company savedCompany = companyServiceImpl.addCompany(company);
		assertThat(savedCompany).isNotNull();
	}

	@Test
	void shouldThrowBadRequestExceptionOnAddCompany() {
		given(companyRepository.findByNameAndAddressAndPostalCodeAndCityAndCountry(company.getName(),
				company.getAddress(), company.getPostalCode(), company.getCity(), company.getCountry()))
						.willReturn(Optional.of(company));

		assertThrows(BadRequestException.class, () -> {
			companyServiceImpl.addCompany(company);
		});
		verify(companyRepository, never()).save(any(Company.class));
	}

	@Test
	void shouldUpdateCompany() {
		given(companyRepository.findById(3L)).willReturn(Optional.of(company));
		given(companyRepository.save(any(Company.class))).willReturn(company);

		company.setName("Banana madness");

		Optional<Company> updatedCompany = companyServiceImpl.updateCompany(3L, company);
		assertThat(updatedCompany.get().getName()).isEqualTo("Banana madness");

	}

	@Test
	void shouldThrowNotFoundExceptionOnUpdateCompany() {

		assertThrows(NotFoundException.class, () -> {
			companyServiceImpl.updateCompany(3L, company);
		});
		verify(companyRepository, never()).save(any(Company.class));

	}

	@Test
	void shouldDeleteCompany() {
		long companyId = 3L;

		given(companyRepository.findById(companyId)).willReturn(Optional.of(company));
		willDoNothing().given(companyRepository).deleteById(companyId);

		companyServiceImpl.deleteCompany(companyId);
		verify(companyRepository, times(1)).deleteById(companyId);
	}

	@Test
	void shouldThrowNotFoundExceptionOnDeleteCompany() {

		long companyId = 3L;

		assertThrows(NotFoundException.class, () -> {
			companyServiceImpl.deleteCompany(companyId);
		});

		verify(companyRepository, never()).deleteById(companyId);

	}

}
