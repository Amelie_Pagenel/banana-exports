package com.example.bananaexport.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.bananaexport.entity.Order;
import com.example.bananaexport.exception.BadRequestException;
import com.example.bananaexport.exception.NotFoundException;
import com.example.bananaexport.repository.OrderRepository;
import com.example.bananaexport.utils.OrderUtils;

import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {

	@Mock
	private OrderRepository orderRepository;

	@Mock
	private OrderUtils orderUtils;

	@InjectMocks
	private OrderServiceImpl orderServiceImpl;

	Order order = new Order();

	@BeforeEach
	public void setUpTest() {

		order.setId(1L);
		order.setCompanyId(2L);
		order.setDeliveryDate(new Date());
		order.setQuantity(25);
		order.setPrice(62.5);

	}

	@Test
	void shouldGetOrdersByCompanyId() {

		List<Order> orders = new ArrayList<Order>();
		orders.add(order);

		given(orderRepository.findByCompanyId(2L)).willReturn(orders);

		List<Order> foundOrders = orderServiceImpl.getOrdersByCompanyId(2L);
		assertThat(foundOrders.size()).isEqualTo(1);

	}

	@Test
	void shouldAddNewOrder() {

		given(orderRepository.save(any(Order.class))).willReturn(order);
		given(orderUtils.isQuantityValid(order.getQuantity())).willReturn(true);

		Order savedOrder = orderServiceImpl.addOrder(order);
		assertThat(savedOrder).isNotNull();

	}

	@Test
	void shouldUpdateOrder() {

		order.setQuantity(100);
		given(orderRepository.findById(1L)).willReturn(Optional.of(order));
		given(orderUtils.isQuantityValid(order.getQuantity())).willReturn(true);
		given(orderUtils.calculateDeliveryPrice(order.getQuantity())).willReturn(250.0);
		given(orderRepository.save(any(Order.class))).willReturn(order);

		Order updatedOrder = orderServiceImpl.updateOrder(1L, order);

		assertThat(updatedOrder.getQuantity()).isEqualTo(100);
	}

	@Test
	void shouldThrowBadRequestExceptionOnOrderUpdate() {

		given(orderRepository.findById(1L)).willReturn(Optional.of(order));
		order.setQuantity(12);

		assertThrows(BadRequestException.class, () -> {
			orderServiceImpl.updateOrder(1L, order);
		});

		verify(orderRepository, never()).save(any(Order.class));

	}

	@Test
	void shouldThrowNotFoundExceptionOnOrderUpdate() {

		order.setQuantity(12);

		assertThrows(NotFoundException.class, () -> {
			orderServiceImpl.updateOrder(1L, order);
		});

		verify(orderRepository, never()).save(any(Order.class));

	}

	@Test
	void shouldDeleteOrder() {
		long orderid = 1L;

		given(orderRepository.findById(1L)).willReturn(Optional.of(order));
		willDoNothing().given(orderRepository).deleteById(orderid);

		orderServiceImpl.deleteOrder(orderid);
		verify(orderRepository, times(1)).deleteById(orderid);

	}

	@Test
	void shouldThrowNotFoundExceptionOnOrderDelete() {

		long orderid = 1L;

		assertThrows(NotFoundException.class, () -> {
			orderServiceImpl.deleteOrder(orderid);
		});

		verify(orderRepository, never()).deleteById(orderid);

	}

}
